import React, { useRef } from 'react'
import { withRouter } from 'react-router-dom'

import TopContent from './topContent'
import BottomContent from './bottomContent'
import { HOCQueryMutation } from '../../components/util'
import { SHOP_BY_ID } from './_query'

import './index.less'


function ShopPage (props) {
  console.log(props)
  const { data: { shopById, menuByShopId }, match } = props
  console.log(props)
  return (
    <div className='wrapper'>
      <TopContent data={shopById} />
      <BottomContent shopId={match.params.id} data={menuByShopId} />
      <div style={{ height: '50px' }} />
    </div>
  )
}

export default HOCQueryMutation([
  {
    query: SHOP_BY_ID,
    name: 'shopById',
    options: (props) => ({
      variables: {
        _id: props.match.params.id,
        shopId: props.match.params.id
      }
    })
  }
])(withRouter(ShopPage))