import React from 'react'
import { Row, Col, Avatar, Rate, Divider } from 'antd'

import './index.less'

function TopContent (props) {
  const { data } = props
  const imageUrl = process.env.REACT_APP_imagesUrl
  return (
    <div className='top-content'>
      <Row>
        <Col offset={4} span={6}>
          <Avatar src={`${imageUrl}/${data.imageUrl}`} shape='square' size='large' />
        </Col>
        <Col span={10}>
          <Row>
            <div className='txt-name'>{data.name}</div>
          </Row>
          <Row>
            <div className='txt-address'>{data.address}</div>
          </Row>
          <Row>
            <Rate disabled allowHalf defaultValue={5} />
          </Row>
          <Row>
            <div className='status'>
              <span>Mở cửa</span>
            </div>
          </Row>
          <Divider />
          <Row>
            <div className='utility'>
              <div className='utility-item'>
                <div className='utility-title'>
                  Chuẩn bị
                </div>
                <div className='utility-content'>
                  10 phút
                </div>
              </div>
            </div>
          </Row>
        </Col>
      </Row>
    </div>
  )
}

export default TopContent