import React from 'react'
import { Tabs, Row, Button, Modal } from 'antd'

import Dish from './dish'
import CartDialog from './cartDialog'

import './index.less'

const { TabPane } = Tabs
let refDialog = null

function BottonComtent (props) {
  const { data } = props

  // const menus = data.map((item, idx) => (
  //   <TabPane tab={item.name} key={idx}>
  //     {item.dishes.map((dish, index) => (
  //       <Dish key={index} data={dish} addDish={refDialog.addDish} />
  //     ))}
  //   </TabPane>
  // ))

  const getRef = (value) => {
    refDialog = value
  }

  return (
    <>
      <CartDialog shopId={props.shopId} getRef={getRef} />
      <Row className='cart-btn'>
        <Button icon='shopping-cart' type='primary' size='large' onClick={() => refDialog.setVisibleDialog(true)} />
      </Row>
      <div className='bottom-content'>
        <Row>
          <Tabs type='card' defaultActiveKey='0' tabPosition='left'>
            {data.map((item, idx) => (
              <TabPane tab={item.name} key={idx}>
                {item.dishes.map((dish, index) => (
                  <Dish key={index} data={dish} addDish={(source) => refDialog.addDish(source)} />
                ))}
              </TabPane>
            ))}
          </Tabs>
        </Row>
      </div>
    </>
  )
}

export default BottonComtent