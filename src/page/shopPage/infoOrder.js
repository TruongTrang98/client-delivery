import React from 'react'
import { Table, Avatar } from 'antd'

function InfoOrder (props) {
  const imageUrl = process.env.REACT_APP_imagesUrl

  const columns = [
    {
      title: '',
      dataIndex: 'imageUrl',
      key: 'imageUrl',
      width: 80,
      render: (text) => <Avatar src={`${imageUrl}/${text}`} size='small' shape='square' />
    },
    {
      title: '',
      dataIndex: 'name',
      key: 'name'
    },
    {
      title: '',
      dataIndex: 'count',
      key: 'count',
      width: 50
    },
    {
      title: '',
      dataIndex: 'totalPrice',
      key: 'totalPrice',
      width: 100,
      render: (_, record) => Intl.NumberFormat('vi-VN', {
        style: 'currency',
        currency: 'VND',
        maximumFractionDigits: 2
      }).format(+record.price * record.count)
    }
  ]

  const totalPrice = props.dishes.reduce((curentValue, curentDish) => curentValue + (curentDish.count * curentDish.price), 0)

  return (
    <>
      <div className='list-dishes'>
        <Table pagination={false} dataSource={props.dishes} columns={columns} scroll={{ y: 300 }} />
      </div>
      <div className='totalPrice'>
        Phí ship: &nbsp;
        <span>
          {Intl.NumberFormat('vi-VN', {
            style: 'currency',
            currency: 'VND',
            maximumFractionDigits: 2
          }).format(15000)}
        </span>
      </div>
      <div className='totalPrice'>
        Thành tiền: &nbsp;
        <span>
          {Intl.NumberFormat('vi-VN', {
            style: 'currency',
            currency: 'VND',
            maximumFractionDigits: 2
          }).format(totalPrice + 15000)}
        </span>
      </div>
    </>
  )
}

export default InfoOrder