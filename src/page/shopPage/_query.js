import gql from 'graphql-tag'

export const SHOP_BY_ID = gql`
  query ($_id: String!){
    shopById(_id: $_id){
      _id
      name
      imageUrl
      address
      email
      phoneNumber
      createdAt
      updatedAt
    },
    menuByShopId (shopId: $_id) {
      _id
      name
      dishes{
        _id
        name
        imageUrl
        price
        coupon {
          content
          percent
        }
      }
      isActive
      createdAt
      updatedAt
    }
  }
`

export const ME = gql`
  query {
    me{
      _id
      name
      address
      phoneNumber
    }
  }
`

export const BOOK = gql`
  mutation ($input: OrderInput!) {
    book (input: $input) {
      _id
    }
  }
`