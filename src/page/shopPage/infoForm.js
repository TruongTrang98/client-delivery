import React from 'react'
import { Form, Input } from 'antd'
import gql from 'graphql-tag'

class InfoForm extends React.Component {
  constructor (props) {
    super(props)
    this.state = { loading: false }
  }
  render () {
    const { data, form: { getFieldDecorator, getFieldsValue } } = this.props

    const formItems = [
      {
        field: 'name',
        label: `Tên người nhận`,
        getFieldDecoratorOpts: {
          initialValue: !!data ? data.name : '',
          rules: [
            { required: true, message: `Vui lòng nhập tên` },
            {
              pattern: /^[^\s]/,
              message: 'Không được có dấu cách đầu dòng'
            }
          ]
        },
        propsInput: {
          placeholder: `Tên người nhận`,
          onChange: (e) => this.props.changeInfo({ name: e.target.value })
        }
      },
      {
        field: 'phoneNumber',
        label: `Số điện thoại`,
        getFieldDecoratorOpts: {
          initialValue: !!data ? data.phoneNumber : '',
          rules: [
            { required: true, message: 'Vui lòng nhập số điện thoại' },
            {
              pattern: /((09|03|07|08|05)+([0-9]{8})\b)/g,
              message: 'Số điện thoại không hợp lệ'
            }
          ]
        },
        propsInput: {
          placeholder: `Số điện thoại`,
          onChange: (e) => this.props.changeInfo({ phoneNumber: e.target.value })
        }
      },
      {
        field: 'address',
        label: 'Địa chỉ nhận hàng',
        getFieldDecoratorOpts: {
          initialValue: !!data ? data.address : '',
          rules: [
            { required: true, message: 'Vui lòng nhập địa chỉ' }
          ]
        },
        propsInput: {
          placeholder: 'Địa chỉ nhận hàng',
          onChange: (e) => this.props.changeInfo({ address: e.target.value })
        }
      }
    ].map((item, idx) => (
      <Form.Item key={`${idx}`} label={item.label}>
        {getFieldDecorator(item.field, {
          ...item.getFieldDecoratorOpts
        })(<Input {...item.propsInput} />)}
      </Form.Item>
    ))

    return (
      <Form>
        {formItems}
      </Form>
    )
  }
}

export default Form.create()(InfoForm)