import React from 'react'
import { withRouter } from 'react-router-dom'
import { Row, Col, Avatar, Button, Divider } from 'antd' 

import { HOCMobX, Notify } from '../../components/util'

function Dish (props) {
  const { data, Authen: { isLogin }, history, addDish } = props

  const handleClick = () => {
    if (!isLogin) {
      history.push('/login')
    } else {
      addDish({ ...data, count: 1, key: data._id })
      return new Notify('success', 'Đã thêm vào giỏ hàng')
    }
  }
  const imageUrl = process.env.REACT_APP_imagesUrl
  
  return (
    <>
      <Row>
        <Col span={3}>
          <Avatar src={`${imageUrl}/${data.imageUrl}`} size='small' shape='square' />
        </Col>
        <Col span={7}>
          <div className='item-name'>{data.name}</div>
        </Col>
        <Col offset={6} span={4}>
          {
            data.coupon && (
              <div className='item-old-price' style={{ textDecoration: 'line-through' }}>
                {`Giá gốc: ${Intl.NumberFormat('vi-VN', {
                  style: 'currency',
                  currency: 'VND',
                  maximumFractionDigits: 2
                }).format(+data.price)}`}
              </div>
            )
          }
        </Col>
        <Col span={3}>
          <div className='item-price'>
            {`${Intl.NumberFormat('vi-VN', {
              style: 'currency',
              currency: 'VND',
              maximumFractionDigits: 2
            }).format(data.coupon ? +data.price - (+data.price * (data.coupon.percent / 100)) : +data.price)}`}
          </div>
        </Col>
        <Col offset={1}>
          <Button type='danger' icon='plus' size='small' onClick={handleClick} />
        </Col>
      </Row>
      <Divider />
    </>
  )
}

export default withRouter(HOCMobX(Dish)(true))