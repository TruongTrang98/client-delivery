import React, { Component } from 'react'
import { Modal, Steps, Button, Avatar, Icon, Result, Table } from 'antd'
import { withApollo } from 'react-apollo'

import InfoForm from './infoForm'
import InfoOrder from './infoOrder'
import { ME, BOOK } from './_query'

import './index.less'

const { Step } = Steps

class BookDialog extends Component {
  constructor (props) {
    super(props)
    this.state = {
      visible: false,
      currentStep: 0,
      info: null,
      isSuccess: false,
      isLoading: false,
      orderId: '',
      isError: false
    }
  }

  componentDidMount = () => {
    const { client, getRef } = this.props
    getRef(this)
    client.query({
      query: ME
    }).then(res => {
      const info = res.data.me
      delete info.__typename
      this.setState({ info })
    }).catch(err => {
      console.error(err)
    })
  }

  setVisibleDialog = (value) => {
    this.setState({ visible: value })
  }

  nextStep = () => {
    this.setState(prevState => ({ currentStep: prevState.currentStep + 1 }))
  }

  prevStep = () => {
    this.setState(prevState => ({ currentStep: prevState.currentStep - 1 }))
  }

  handleCloseDialog = () => {
    this.setState({ currentStep: 0 })
    this.setVisibleDialog(false)
    if (this.state.currentStep === 2) {
      this.props.clearCart()
    }
  }

  changeInfo = (newInfo) => {
    this.setState(prevState => {
      const info = { ...prevState.info, ...newInfo }
      let isError = false
      if (!!info.name && !!info.phoneNumber && !!info.address) {
        isError = false
      } else {
        isError = true
      }
      return { info, isError }
    })
  }

  handleBook = () => {
    this.setState({ isLoading: true })
    const { client } = this.props
    const dishes = this.props.dishes
      .map(dish => ({ _id: dish._id, name: dish.name, count: dish.count, price: dish.price }))
    const costDishes = this.props.dishes
      .reduce((curentValue, curentDish) => curentValue + (curentDish.count * curentDish.price), 0)
    const input = { 
      ...this.state.info,
      dishes,
      shopId: this.props.shopId,
      costShip: '15000',
      costDishes: `${costDishes}`,
      userId: this.state.info._id
    }
    delete input._id
    client.mutate({
      mutation: BOOK,
      variables: {
        input
      }
    }).then(res => {
      this.setState(prevState => ({
        isLoading: false,
        orderId: res.data.book._id,
        currentStep: prevState.currentStep + 1
      }))
    }).catch(err => {
      this.setState({
        isLoading: false,
        currentStep: 0
      })
      console.error(err)
    })
  }

  render () {
    const steps = [
      {
        title: 'Cung cấp thông tin',
        content: <InfoForm data={this.state.info} changeInfo={this.changeInfo} />,
        icon: <Icon type='edit' />
      },
      {
        title: 'Kiểm tra lại đơn hàng',
        content: <InfoOrder dishes={this.props.dishes} />,
        icon: <Icon type='book' />
      },
      {
        title: 'Hoàn tất',
        content: (
          <Result
            status='success'
            title='Đặt hàng thành công'
            subTitle={`Mã đơn hàng của bạn là ${this.state.orderId}. Vui lòng đợi thông báo để biết tình trạng của đơn hàng`}
            extra={[
              <Button type='primary' onClick={this.handleCloseDialog} key='buy'>Tiếp tục đặt món</Button>
            ]}
          />
        ),
        icon: <Icon type='smile-o' />
      }
    ]

    const { currentStep, visible } = this.state
    return (
      <Modal
        footer={null}
        visible={visible}
        onCancel={this.handleCloseDialog}
      >
        <Steps current={currentStep} style={{ marginTop: 15 }}>
          {steps.map(item => (
            <Step key={item.title} title={item.title} icon={item.icon} />
          ))}
        </Steps>
        <div className='steps-content'>{steps[currentStep].content}</div>
        <div className='footerCustom'>
          {currentStep > 0 && currentStep !== 2 && (
            <Button style={{ marginRight: 8 }} onClick={this.prevStep}>
              Quay lại
            </Button>
          )}
          {currentStep === 0 && (
            <Button type='primary' onClick={this.nextStep} disabled={this.state.isError}>
              Tiếp tục
            </Button>
          )}
          {currentStep === 1 && (
            <Button loading={this.state.isLoading} type='primary' onClick={this.handleBook}>
              Đặt hàng
            </Button>
          )}
          {currentStep === 2 && (
            <Button type='primary' onClick={this.handleCloseDialog}>
              Hoàn tất
            </Button>
          )}
        </div>
      </Modal>
    )
  }
}

export default withApollo(BookDialog)