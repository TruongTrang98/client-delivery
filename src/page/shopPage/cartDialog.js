import React from 'react'
import { Modal, Table, Empty, Button, Avatar } from 'antd'

import BookDialog from './bookDialog'

import './index.less'

let refDialog = null

class CartDialog extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      visible: false,
      dishes: []
    }
  }

  componentDidMount = () => {
    this.props.getRef(this)
  }

  handleOk = () => {
    this.setState({ visible: false })
  }

  setVisibleDialog = (value) => {
    this.setState({ visible: value })
  }

  getRefDialog = (value) => {
    refDialog = value
  }

  addDish = (dish) => {
    const newDish = { ...dish, price: dish.coupon ? `${+dish.price - (+dish.price * (dish.coupon.percent / 100))}` : dish.price }
    delete newDish.coupon
    const index = this.state.dishes.findIndex(item => item._id === newDish._id)
    let newDishes = [...this.state.dishes]
    if (index > -1) {
      newDishes[index].count += 1
    } else {
      newDishes.push(newDish)
    }
    this.setState(_ => ({ dishes: newDishes }))
  }

  increaseCount = (_id) => {
    const index = this.state.dishes.findIndex(dish => dish._id === _id)
    if (index > -1) {
      this.state.dishes[index].count += 1
      this.setState(prevState => ({ dishes: prevState.dishes }))
    }
  }

  decreaseCount = (_id) => {
    const index = this.state.dishes.findIndex(dish => dish._id === _id)
    if (index > -1) {
      this.state.dishes[index].count -= 1
      this.setState(prevState => ({ dishes: prevState.dishes }))
    }
  }

  removeDish = (_id) => {
    const index = this.state.dishes.findIndex(dish => dish._id === _id)
    if (index > -1) {
      this.state.dishes.splice(index, 1)
      this.setState(prevState => ({ dishes: prevState.dishes }))
    }
  }

  handleBook = () => {
    this.setVisibleDialog(false)
    refDialog.setVisibleDialog(true)
  }

  clearCart = () => {
    this.setState({ dishes: [] })
  }

  render () {
    const imageUrl = process.env.REACT_APP_imagesUrl

    const columns = [
      {
        title: '',
        dataIndex: 'imageUrl',
        key: 'imageUrl',
        width: 80,
        render: (text) => <Avatar src={`${imageUrl}/${text}`} size='small' shape='square' />
      },
      {
        title: '',
        dataIndex: 'name',
        key: 'name'
      },
      {
        title: '',
        dataIndex: 'totalPrice',
        key: 'totalPrice',
        width: 100,
        render: (_, record) => Intl.NumberFormat('vi-VN', {
          style: 'currency',
          currency: 'VND',
          maximumFractionDigits: 2
        }).format(+record.price * record.count)
      },
      {
        title: '',
        dataIndex: 'minus_btn',
        key: 'minus_btn',
        width: 50,
        render: (_, record) => <Button type='danger' disabled={record.count <= 1} icon='minus' size='small' onClick={() => this.decreaseCount(record._id)} />
      },
      {
        title: '',
        dataIndex: 'count',
        key: 'count',
        width: 50
      },
      {
        title: '',
        dataIndex: 'plus_btn',
        key: 'plus_btn',
        width: 50,
        render: (_, record) => <Button type='primary' icon='plus' size='small' onClick={() => this.increaseCount(record._id)} />
      },
      {
        title: '',
        dataIndex: 'remove',
        key: 'remove',
        render: (_, record) => <Button size='small' type='link' onClick={() => this.removeDish(record._id)}>Xóa khỏi giỏ hàng</Button>
      }
    ]

    const totalPrice = this.state.dishes.reduce((curentValue, curentDish) => curentValue + (curentDish.count * curentDish.price), 0)

    return (
      <>
        <Modal
          title='Giỏ đồ'
          visible={this.state.visible}
          footer={null}
          onCancel={() => this.setVisibleDialog(false)}
        >
          {this.state.dishes.length === 0 
            ? (<Empty description='Không có món ăn' />)
            : (
              <>
                <div className='list-dishes'>
                  <Table pagination={false} dataSource={this.state.dishes} columns={columns} scroll={{ y: 300 }} />
                </div>
                <div className='totalPrice'>
                  Thành tiền: &nbsp;
                  <span>
                    {Intl.NumberFormat('vi-VN', {
                      style: 'currency',
                      currency: 'VND',
                      maximumFractionDigits: 2
                    }).format(totalPrice)}
                  </span>
                </div>
                <div className='footerCustom'>
                  <Button icon='book' className='btn-book' type='primary' onClick={this.handleBook}>Tiến hành đặt hàng</Button>
                </div>
              </>
            )}
        </Modal>
        <BookDialog shopId={this.props.shopId} getRef={this.getRefDialog} clearCart={this.clearCart} dishes={this.state.dishes} />
      </>
    )
  }
}

export default CartDialog