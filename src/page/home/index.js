import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Header from '../../components/Header'
import Footer from '../../components/Footer'
import Content from '../../components/Content/Content'

function HomePage (props) {
  return (
    <div>
      {/* <Header /> */}
      <Content />
      {/* <Footer /> */}
    </div>
  )
}

export default HomePage