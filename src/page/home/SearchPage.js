import React, { Component } from 'react'
import { Row, Col, Card, Pagination } from 'antd'

import Header from '../../components/Header'
import Footer from '../../components/Footer'

const { Meta } = Card

class ShopPage extends Component {
  // state = {
  //   current: 3
  // }

  // onChange = page => {
  //   console.log(page)
  //   this.setState({
  //     current: page
  //   })
  // }

  render () {
    return (
      <div>
        <Header />
        <hr />
        <div style={{ width: 1284, margin: '0 auto' }}>
          <h1>Danh sách các món ăn</h1>
          <Row>
            <Col span={6} style={{ marginTop: 15 }}>
              <Card
                style={{ margin: '0 5px 0 5px' }}
                hoverable
                cover={
                  (
                    <img
                      alt='example'
                      src='https://www.foodiesfeed.com/wp-content/uploads/2019/06/beautiful-vibrant-shot-of-traditional-korean-meals-413x275.jpg'
                      style={{ height: 170 }}
                    />
                  )
                }
              >
                <Meta
                  title='Cơm nhà làm, ngon như nhà làm'
                  description='310/15/9 Dương Quảng Hàm, Gò Vấp'
                />
              </Card>
            </Col>
            <Col span={6} style={{ marginTop: 15 }}>
              <Card
                style={{ margin: '0 5px 0 5px' }}
                hoverable
                cover={
                  (
                    <img
                      alt='example'
                      src='https://www.foodiesfeed.com/wp-content/uploads/2019/06/beautiful-vibrant-shot-of-traditional-korean-meals-413x275.jpg'
                      style={{ height: 170 }}
                    />
                  )
                }
              >
                <Meta
                  title='Cơm nhà làm, ngon như nhà làm'
                  description='310/15/9 Dương Quảng Hàm, Gò Vấp'
                />
              </Card>
            </Col>
          </Row>
          <Pagination
            current={this.state.current}
            onChange={this.onChange}
            total={50}
            style={{ textAlign: 'center', marginTop: 50 }}
          />
        </div>

        <Footer />
      </div>
    )
  }
}

export default ShopPage
