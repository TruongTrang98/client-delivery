import React, { Component } from 'react'
import './SignIn.css'
import { Form, Select, Icon, Input, Button, Card } from 'antd'
import gql from 'graphql-tag'
import * as jwt from 'jsonwebtoken'
import { Link } from 'react-router-dom'
import { Notify, HOCMobX, HOCQueryMutation } from '../../components/util'

const { Option } = Select

class SignIn extends Component {
  constructor (props) {
    super(props)
    this.state = {
      loading: false
    }
  }

  handleLogin = () => {
    const { form: { getFieldsValue, setFields, getFieldValue }, mutate: { login }, Authen: { onLogin } } = this.props
    this.setState({ loading: true })
    login({
      variables: {
        input: getFieldsValue()
      }
    }).then(res => {
      console.log(res)
      const token = jwt.verify(res.data.userLogin.token, 'delivery')
      console.log(token)
      if (token.role === 'user') {
        localStorage.setItem('username', getFieldValue('username'))
        onLogin(res.data.userLogin.token)
        this.setState({ loading: false })
        // getUserProfile()
        return new Notify('success', 'Đăng nhập thành công')
      } else {
        this.setState({ loading: false })
        setFields({
          username: {
            errors: [new Error('Tên đăng nhập hoặc mật khẩu sai. Vui lòng nhập lại')]
          },
          password: {
            errors: [new Error('Tên đăng nhập hoặc mật khẩu sai. Vui lòng nhập lại')]
          }
        })
        return new Notify('error', 'Đăng nhập thất bại')
      }
    }).catch(err => {
      this.setState({ loading: false })
      console.error(err)
      setFields({
        username: {
          errors: [new Error('Tên đăng nhập hoặc mật khẩu sai. Vui lòng nhập lại')]
        },
        password: {
          errors: [new Error('Tên đăng nhập hoặc mật khẩu sai. Vui lòng nhập lại')]
        }
      })
      return new Notify('error', 'Đăng nhập thất bại')
    })
  }

  validateToNextPassword = (rule, value, callback) => {
    const { form } = this.props
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true })
    }
    if (!/^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{8,}$/.test(value)) {
      callback('Mật khẩu không đúng yêu cầu')
    }
    callback()
  }

  render () {
    const { getFieldDecorator } = this.props.form
    console.log(this.props)
    return (
      <div className='card-login'>
        <Card style={{ width: 360, height: 330 }}>
          <h1 style={{ marginBottom: 20 }}>ĐĂNG NHẬP</h1>
          <Form onSubmit={this.handleSubmit}>
            <Form.Item>
              {getFieldDecorator('username', {
                rules: [
                  {
                    type: 'email',
                    message: 'Email không đúng định dạng!'
                  },
                  {
                    required: true,
                    message: 'Vui lòng nhập Email!'
                  }
                ]
              })(
                <Input
                  prefix={
                    <Icon type='user' style={{ color: 'rgba(0,0,0,.25)' }} />
                  }
                  placeholder='Tên đăng nhập (Email)'
                />
              )}
            </Form.Item>
            <Form.Item
              hasFeedback
              help='Mật khẩu phải ít nhất 8 ký tự, bao gồm cả số và chữ cái'
              style={{ textAlign: 'left' }}
            >
              {getFieldDecorator('password', {
                rules: [
                  { required: true, message: 'Vui lòng nhập mật khẩu!' },
                  {
                    validator: this.validateToNextPassword
                  }
                ]
              })(
                <Input.Password
                  prefix={
                    <Icon type='lock' style={{ color: 'rgba(0,0,0,.25)' }} />
                  }
                  type='password'
                  placeholder='Mật khẩu'
                />
              )}
            </Form.Item>
            <Form.Item style={{ marginTop: 10 }}>
              <Button
                loading={this.state.loading}
                type='primary'
                className='login-form-button'
                onClick={this.handleLogin}
              >
                Đăng nhập
              </Button>
              <br />
              <Link to={`signUp`}> Quên mật khẩu </Link>
              <br />
              <Link to={`signUp`}> Chưa có tài khoản. Đăng ký ngay! </Link>
            </Form.Item>
          </Form>
        </Card>
      </div>
    )
  }
}
const WrappedNormalSignIn = Form.create({ name: 'normal_login' })(HOCMobX(SignIn)(true))

const USER_LOGIN = gql`
  mutation ($input: LoginInput!){
    userLogin(input: $input){
      token
    }
  }
`

export default HOCQueryMutation([{ mutation: USER_LOGIN, name: 'login' }])(WrappedNormalSignIn)
