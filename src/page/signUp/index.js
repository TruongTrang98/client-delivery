import React, { Component } from 'react'
import { Form, Select, Icon, Input, Button, Card } from 'antd'
import gql from 'graphql-tag'
import * as jwt from 'jsonwebtoken'
import { Link } from 'react-router-dom'
import { Notify, HOCMobX, HOCQueryMutation } from '../../components/util'


import './SignUp.css'

const { Option } = Select

class SignUp extends Component {
  constructor (props) {
    super(props)
    this.state = {
      loading: false
    }
  }

  handleSignUp = async () => {
    const { form: { getFieldsValue, setFields }, mutate: { login, createUser }, Authen: { onLogin } } = this.props
    const input = { ...getFieldsValue() }
    delete input.confirm
    this.setState({ loading: true })
    try {
      const resCreateUser = await createUser({
        variables: {
          input
        }
      })
      if (resCreateUser.data.createUser === '200') {
        login({
          variables: {
            input: { 
              username: input.email,
              password: input.password
            }
          }
        }).then(res => {
          const token = jwt.verify(res.data.userLogin.token, 'delivery')
          if (token.role === 'user') {
            localStorage.setItem('username', input.email)
            onLogin(res.data.userLogin.token)
            this.setState({ loading: false })
            // getUserProfile()
            return new Notify('success', 'Đăng kí thành công. Tự động đăng nhập')
          } else {
            return new Notify('error', 'Đăng kí thất bại')
          }
        }).catch(err => {
          this.setState({ loading: false })
          console.error(err)
          return new Notify('error', 'Đăng kí thất bại')
        })
      } else if (resCreateUser.data.createUser === '410') {
        this.setState({ loading: false })
        setFields({
          email: {
            errors: [new Error('Email đã được sử dụng. Vui lòng chọn email khác')]
          }
        })
        return
      } else {
        this.setState({ loading: false })
        return new Notify('error', 'Đăng kí thất bại')
      }
    } catch (error) {
      this.setState({ loading: false })
      console.error(error)
      return new Notify('error', 'Đăng kí thất bại')
    }
  }

  validateToNextPassword = (rule, value, callback) => {
    const { form } = this.props
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true })
    }
    if (!/^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{8,}$/.test(value)) {
      callback('Mật khẩu không đúng yêu cầu')
    }
    callback()
  }

  validatePhoneNumber= (rule, value, callback) => {
    if (value === null || value === '') {
      callback()
    } else {
      if (!/((09|03|07|08|05)+([0-9]{8})\b)/g.test(value)) {
        callback('Số điện thoại không đúng định dạng!')
      }
    }
    callback()
  }

  compareToFirstPassword = (rule, value, callback) => {
    const { form } = this.props
    if (value && value !== form.getFieldValue('password')) {
      callback('Mật khẩu không trùng khớp')
    } else {
      callback()
    }
  }

  render () {
    const { getFieldDecorator, getFieldValue } = this.props.form
    return (
      <div className='card-login'>
        <Card style={{ width: 360, height: 605 }}>
          <h1 style={{ marginBottom: 20 }}>ĐĂNG KÝ TÀI KHOẢN</h1>
          <Form onSubmit={this.handleSubmit}>
            <Form.Item>
              {getFieldDecorator('email', {
                rules: [
                  {
                    type: 'email',
                    message: 'Email không đúng định dạng!'
                  },
                  {
                    required: true,
                    message: 'Vui lòng nhập Email!'
                  }
                ]
              })(
                <Input
                  prefix={
                    <Icon type='user' style={{ color: 'rgba(0,0,0,.25)' }} />
                  }
                  placeholder='Tên đăng nhập (Email)'
                />
              )}
            </Form.Item>
            <Form.Item>
              {getFieldDecorator('name', {
                rules: [
                  {
                    required: true,
                    message: 'Vui lòng nhập tên của bạn'
                  }
                ]
              })(<Input placeholder='Họ và tên' />)}
            </Form.Item>
            <Form.Item>
              {getFieldDecorator('gender', {
                rules: [{ required: true, message: 'Vui lòng chọn giới tính!' }]
              })(
                <Select
                  placeholder='Giới tính'
                  //   onChange={this.handleSelectChange}
                >
                  <Option value='Nam'>Nam</Option>
                  <Option value='Nữ'>Nữ</Option>
                  <Option value='Khác'>Khác</Option>
                </Select>
              )}
            </Form.Item>
            <Form.Item>
              {getFieldDecorator('phoneNumber', {
                rules: [
                  { required: true, message: 'Vui lòng nhập số điện thoại!' },
                  {
                    validator: this.validatePhoneNumber
                  }
                ]
              })(
                <Input style={{ width: '100%' }} placeholder='Số điện thoại' />
              )}
            </Form.Item>
            <Form.Item>
              {getFieldDecorator('address', {
                rules: [
                  {
                    required: true,
                    message: 'Vui lòng nhập địa chỉ'
                  }
                ]
              })(<Input placeholder='Địa chỉ' />)}
            </Form.Item>
            <Form.Item
              hasFeedback
              help='Mật khẩu phải ít nhất 8 ký tự, bao gồm cả số và chữ cái'
              style={{ textAlign: 'left' }}
            >
              {getFieldDecorator('password', {
                rules: [
                  { required: true, message: 'Vui lòng nhập mật khẩu!' },
                  {
                    validator: this.validateToNextPassword
                  }
                ]
              })(
                <Input.Password
                  prefix={
                    <Icon type='lock' style={{ color: 'rgba(0,0,0,.25)' }} />
                  }
                  type='password'
                  placeholder='Mật khẩu'
                />
              )}
            </Form.Item>
            <Form.Item hasFeedback>
              {getFieldDecorator('confirm', {
                rules: [
                  {
                    required: true,
                    message: 'Vui lòng xác nhận đúng mật khẩu!'
                  },
                  {
                    validator: this.compareToFirstPassword
                  }
                ]
              })(
                <Input.Password
                  prefix={
                    <Icon type='lock' style={{ color: 'rgba(0,0,0,.25)' }} />
                  }
                  type='password'
                  placeholder='Xác nhận mật khẩu'
                />
              )}
            </Form.Item>
            <Form.Item>
              <Button
                loading={this.state.loading}
                type='primary'
                htmlType='submit'
                className='login-form-button'
                onClick={this.handleSignUp}
              >
                Đăng ký tài khoản
              </Button>
              <p><Link to={`login`}> Đã có tài khoản. Đăng nhập ngay! </Link></p>
            </Form.Item>
          </Form>
        </Card>
      </div>
    )
  }
}
const WrappedNormalSignUp = Form.create({ name: 'normal_login' })(HOCMobX(SignUp)(true))

const CREATE_USER = gql`
  mutation ($input: UserInput!) {
    createUser(input: $input)
  }
`
const USER_LOGIN = gql`
  mutation ($input: LoginInput!){
    userLogin(input: $input){
      token
    }
  }
`

export default HOCQueryMutation([
  { 
    mutation: USER_LOGIN, 
    name: 'login' 
  },
  {
    mutation: CREATE_USER,
    name: 'createUser'
  }
])(WrappedNormalSignUp)
