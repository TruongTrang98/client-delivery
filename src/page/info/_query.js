import gql from 'graphql-tag'


export const ME = gql`
  query {
    me{
      _id
      name
      gender
      address
      email
      phoneNumber
      imageUrl
      createdAt
      updatedAt
    }
  }
`

export const ORDER_BY_USER = gql`
  query {
    orderByUser {
      _id
      dishes {
        _id
        name
        count
        price
      }
      status
      shop {
        _id
        name
        address
      }
      costShip
      costDishes
      createdAt
      updatedAt
    }
  }
`

export const UPDATE_IMAGE = gql`
  mutation ($_id: String!, $image: ImageInput!){
    updateImage(_id: $_id, image: $image)
  }
`

export const UPDATE_USER = gql`
  mutation ($_id: String!, $input: UserInput!){
    updateUserInfo(_id: $_id, input: $input)
  }
`