import React from 'react'
import { Table, Button } from 'antd'
import moment from 'moment'

import { HOCQueryMutation } from '../../components/util'
import { ORDER_BY_USER } from './_query'
import './index.less'

function BottomContent (props) {
  console.log(props.data)
  const columnDef = [
    {
      title: 'Mã đơn hàng',
      dataIndex: '_id',
      key: '_id'
    },
    {
      title: 'Ngày đặt', 
      dataIndex: 'createdAt', 
      key: 'createdAt',
      render: (text) => (
        <span>{moment(+text).format('DD/MM/YYYY')}</span>
      )
    },
    {
      title: 'Shop',
      dataIndex: 'shop.name',
      key: 'shop.name'
    },
    {
      title: 'Phí ship',
      dataIndex: 'costShip',
      key: 'costShip'
    },
    {
      title: 'Tổng tiền',
      dataIndex: 'totalCost',
      key: 'totalCost',
      render: (_, record) => (
        <span>{(+record.costShip) + (+record.costDishes)}</span>
      )
    },
    {
      title: 'Tình trạng đơn hàng',
      dataIndex: 'status',
      key: 'status',
      render: (text) => (
        <Button type='danger' disabled={text !== 'pending'}>
          {text === 'pending' ? 'Đang chờ xử lí'
            : text === 'shopReceiveOrder' ? 'Shop đã tiếp nhận đơn hàng' 
              : text === 'shipperReceiveOrder' ? 'Shipper đã tiếp nhận đơn hàng'
                : text === 'shipperReceiveFood' ? 'Shipper đang đến chỗ của bạn'
                  : text === 'done' ? 'Hoàn tất'
                    : text === 'cancel' ? 'Đơn hàng bị hủy'
                      : null}
        </Button>
      )
    }
  ]

  const expandRow = (record) => {
    const columns = [
      {
        title: 'Tên món ăn',
        dataIndex: 'name',
        key: 'name'
      },
      {
        title: 'Số lượng',
        dataIndex: 'count',
        key: 'count'
      },
      {
        title: 'Giá',
        dataIndex: 'price',
        key: 'price'
      }
    ]
    return <Table columns={columns} dataSource={record} pagination={false} />
  }
  return (
    <>
      <div className='order-label'>Đơn hàng</div>
      <div className='order-wrapper'>
        <div className='order-content'>
          <Table
            columns={columnDef}
            dataSource={props.data.orderByUser}
            expandedRowRender={(record) => expandRow(record.dishes)}
            pagination={false}
          />
        </div>
      </div>
    </>
  )
}

export default HOCQueryMutation([
  {
    query: ORDER_BY_USER,
    name: 'orderByUser'
  }
])(BottomContent)