import React from 'react'

import TopContent from './topContent'
import BottomContent from './bottomContent'

function Info () {
  return (
    <>
      <div className='wrapper'>
        <TopContent />
        <BottomContent />
      </div>
    </>
  )
}

export default Info