import React from 'react'
import { Modal, Form, Button } from 'antd'
import ImageUploader from 'react-images-upload'

import './index.less'

class AvatarDialog extends React.Component {
  constructor (props) {
    super(props)
    this.state = { 
      visibleDialog: false,
      imageBase64: '',
      type: '',
      isLoading: false,
      imageUploadKey: 0
    }
  }

  componentDidMount () {
    this.props.getRef(this)
  }
  
  onDrop = async (picture) => {
    if (picture && picture[0]) {
      let imageBase64 = await this.getBase64(picture[0])
      this.setState({ imageBase64, type: picture[0].type })
    } else {
      this.setState({ imageBase64: '', type: '' })
    }
  }
      
  getBase64 = (file) => new Promise((resolve, reject) => {
    const reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onload = () => resolve(reader.result)
    reader.onerror = error => reject(error)
  })
  
  
  setVisibleDialog = (value) => {
    this.setState({ visibleDialog: value })
  }

  handleCancel = () => {
    this.setVisibleDialog(false)
    this.setState({ imageBase64: '', type: '' })
    this.setState(prev => ({ imageUploadKey: prev.imageUploadKey + 1 })) 
  }

  handleUpdate = async () => {
    this.setState({ isLoading: true })
    await this.props.handleUpdate({ data: this.state.imageBase64, type: this.state.type })
    this.setState(prev => ({
      imageBase64: '', 
      type: '', 
      isLoading: false, 
      visibleDialog: false, 
      imageUploadKey: prev.imageUploadKey + 1 
    }))
    this.props.getRef(this)
  }
  
  render = () => {
    let { visibleDialog, imageBase64, isLoading } = this.state
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 5 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 19 }
      }
    }
    return (
      <>
        <Modal
          footer={null}
          title='Cập nhật ảnh'
          visible={visibleDialog}
          width={550}
          className='lock-account'
          onCancel={this.handleCancel}
        >
          <Form labelAlign={'center'} {...formItemLayout}>
            <ImageUploader
              key={this.state.imageUploadKey}
              withIcon
              singleImage
              withPreview
              buttonText='Chọn file'
              onChange={this.onDrop}
              buttonClassName={imageBase64 ? 'disabledUpload' : ''}
              imgExtension={['.jpg', '.gif', '.png', '.gif']}
              label={'Chọn ảnh từ máy tính của bạn hoặc kéo thả vào đây'}
              labelStyles={{
                width: 200,
                textAlign: 'center'
              }}
              maxFileSize={5242880}
              buttonStyles={{
                color: '#fff',
                borderRadius: 3,
                backgroundColor: '#1890ff',
                borderColor: '#1890ff',
                textShadow: '0 -1px 0 rgba(0, 0, 0, 0.12)',
                boxShadow: '0 2px 0 rgba(0, 0, 0, 0.045)',
                lineHeight: 1.499,
                position: 'relative',
                display: 'inline-block',
                fontWeight: 400,
                whiteSpace: 'nowrap',
                textAlign: 'center',
                backgroundImage: 'none',
                border: '1px solid transparent'
              }}
            />
          </Form>
          <div style={{ height: 20 }} />
          <div style={{ paddingRight: 20 }}>
            <Button
              type='primary'
              style={{ float: 'right' }}
              disabled={!imageBase64}
              onClick={this.handleUpdate}
              loading={isLoading}
            >
                Cập nhật
            </Button>
          </div>
          <div style={{ height: 20 }} />
        </Modal>
      </>
    )
  }
}

export default AvatarDialog
