import React, { useState } from 'react'
import { Avatar, Col, Row, Tag, Button } from 'antd'
import { Camera } from 'react-feather'
import moment from 'moment'

import { UPDATE_IMAGE, UPDATE_USER, ME } from './_query'
import { HOCQueryMutation, Notify } from '../../components/util'
import AvaterDialog from './AvatarDialog'
import InfoDialog from './infoDialog'

import './index.less'

let refAvatar = null
let refDialog = null

function TopContent ({ data, mutate }) {
  console.log(data)
  const [hashImage, setHashImage] = useState(Date.now())
  
  const getRefAvatar = (value) => {
    refAvatar = value
  }

  const getRefDialog = (value) => {
    refDialog = value
  }

  async function handleUpdateImage (image) {
    try {
      const res = await mutate.updateImage({
        variables: {
          _id: data.me._id,
          image
        }
      })
      if (res.data.updateImage === '200') {
        setHashImage(Date.now())
        await data.refetch()
        return new Notify('success', 'Cập nhật ảnh thành công')
      } else {
        return new Notify('error', 'Cập nhật ảnh thất bại')
      }
    } catch (error) {
      console.error(error)
      return new Notify('error', 'Cập nhật ảnh thất bại')
    }
  }

  const handleUpdateInfo = async (input) => {
    try {
      const res = await mutate.updateUser({
        variables: {
          _id: data.me._id,
          input
        }
      })
      if (res.data.updateUserInfo === '200') {
        await data.refetch()
        return new Notify('success', 'Cập nhật thành công')
      } else {
        return new Notify('error', 'Cập nhật thất bại')
      }
    } catch (error) {
      console.error(error)
      return new Notify('error', 'Cập nhật thất bại')
    } 
  }

  const dataInfo = [
    { name: 'Họ và tên', value: data.me.name, visible: true },
    { name: 'Giới tính', value: data.me.gender, visible: true },
    { name: 'Email', value: data.me.email, visible: true },
    { name: 'Địa chỉ', value: data.me.address, visible: true },
    { name: 'Số điện thoại', value: data.me.phoneNumber, visible: true },
    { name: 'Giới tính', value: data.me.gender, visible: data.type === 'user' || data.type === 'shipper' },
    { name: 'Ngày tạo', value: moment(+data.me.createdAt).format('DD/MM/YYYY'), visible: true },
    { name: 'Cập nhật cuối', value: moment(+data.me.updatedAt).format('DD/MM/YYYY'), visible: true }
  ]
  const showData = dataInfo.map((prop, index) => (
    prop.visible && (
      <Row key={index.toString()} style={{ padding: 12 }}>
        <Col span={8}>
          <span style={{ fontWeight: 'bold' }}>{prop.name}</span>
        </Col>
        <Col span={8}>
          <span {...prop.active && { style: { color: 'red' } }}>{prop.value}</span>
        </Col>
      </Row>
    )
  ))
  
  const imageLink = process.env.REACT_APP_imagesUrl
  console.log(data)
  const imageUrl = process.env.REACT_APP_imagesUrl
  console.log(imageUrl)
  return (
    <>
      <div className='info-label'>Thông tin cá nhân</div>
      <div className='info-wrapper'>
        <div className='info-content'>
          <InfoDialog handleUpdateInfo={handleUpdateInfo} data={data.me} getRef={getRefDialog} />
          <Row gutter={{ xs: 8, sm: 16, md: 24 }} className='change-profile-btn'>
            <Button style={{ marginRight: '10px' }} type='link' onClick={() => refDialog.setVisibleDialog(true)}>Cập nhật thông tin cá nhân</Button>
          </Row>
          <Row gutter={{ xs: 8, sm: 16, md: 24 }}>
            {/* avatar */}
            <Col offset={5} span={4}>
              <div className='boxAvatar'>
                <Avatar 
                  shape='square' 
                  size='large'
                  src={`${imageUrl}/${data.me.imageUrl}?${hashImage}`}
                  style={{ cursor: 'pointer' }}
                  onMouseEnter={() => {
                    const imageIcon = document.getElementById('image_icon')
                    if (imageIcon) {
                      document.getElementById('image_icon').style.color = 'white'
                    }
                  }}
                  onMouseLeave={() => {
                    const imageIcon = document.getElementById('image_icon')
                    if (imageIcon) {
                      document.getElementById('image_icon').style.color = 'transparent'
                    }
                  }}
                  onClick={() => refAvatar.setState({ visibleDialog: true })}
                />
                <Camera
                  id='image_icon'
                  style={{
                    height: 35,
                    width: 35,
                    position: 'absolute',
                    top: 61,
                    left: 72,
                    color: 'transparent',
                    cursor: 'pointer'
                  }}
                  onMouseEnter={() => {
                    const imageIcon = document.getElementById('image_icon')
                    if (imageIcon) {
                      document.getElementById('image_icon').style.color = 'white'
                    }
                  }}
                  onMouseLeave={() => {
                    const imageIcon = document.getElementById('image_icon')
                    if (imageIcon) {
                      document.getElementById('image_icon').style.color = 'transparent'
                    }
                  }}
                  onClick={() => refAvatar.setState({ visibleDialog: true })}
                />
              </div>
              <AvaterDialog getRef={getRefAvatar} handleUpdate={handleUpdateImage} />
            </Col>
            <Col offset={1} span={9}>
              {showData}
            </Col>
          </Row>
        </div>
      </div>
    </>
  )
}

export default HOCQueryMutation([
  {
    query: ME,
    name: 'me'
  },
  {
    mutation: UPDATE_IMAGE,
    name: 'updateImage',
    options: {}
  },
  {
    mutation: UPDATE_USER,
    name: 'updateUser'
  }
])(TopContent)