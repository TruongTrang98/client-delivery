import React from 'react'
import { Row, Col, Icon } from 'antd'

import './index.less'

const Notification = (props) => (
  <div className='notifi-tag'>
    <Row>
      <Col span={2}>
        <Icon type='check-circle' />
      </Col>
      <Col span={20}>
        <Row>
          <div className='notifi-content'>
            abc
          </div>
        </Row>
        <Row>
          <div className='notifi-time'>
            abc
          </div>
        </Row>
      </Col>
    </Row>
  </div>
)

function LeftSide (props) {
  return (
    <div className='notifi-wrapper'>
      <Notification />
    </div>
  )
}

export default LeftSide