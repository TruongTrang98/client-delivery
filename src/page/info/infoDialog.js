import React, { useState } from 'react'
import { Input, Button, Form, Radio, Modal } from 'antd'
import './index.less'

const hasErrors = (fieldsError, fieldsValue) => (
  Object.keys(fieldsError).some(field => fieldsError[field])
  || Object.values(fieldsValue).some(field => !field)
)

class InfoDialog extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      loading: false,
      visible: false
    }
  }

  componentDidMount = () => {
    this.props.getRef(this)
  }
  
  handleSubmit = async () => {
    this.setState({ loading: true })
    const { getFieldsValue } = this.props.form
    await this.props.handleUpdateInfo({ ...getFieldsValue(), password: '' })
    this.setState({
      loading: false,
      visible: false
    })
  }

  setVisibleDialog = (value) => {
    this.setState({ visible: value })
  }

  render () {
    const { getFieldDecorator, getFieldsValue, getFieldsError } = this.props.form
    const { data } = this.props

    const formItems = [
      {
        field: 'name',
        type: 'text',
        label: `Họ và tên`,
        getFieldDecoratorOpts: {
          initialValue: data.name || '',
          rules: [
            { required: true, message: `Vui lòng nhập họ và tên` },
            {
              pattern: /^[^\s]/,
              message: 'Không được có dấu cách đầu dòng'
            }
          ]
        },
        propsInput: {
          placeholder: `Họ và tên`
        }
      },
      {
        field: 'gender',
        type: 'radio',
        label: 'Giới tính',
        getFieldDecoratorOpts: {
          initialValue: data.gender || 'Nam',
          rules: [{ required: true, message: 'Vui lòng chọn giới tính' }]
        },
        propsInput: {}
      },
      {
        field: 'email',
        type: 'text',
        label: 'Email',
        getFieldDecoratorOpts: {
          initialValue: data.email || '',
          rules: [
            { required: true, message: 'Vui lòng nhập địa chỉ email' },
            {
              type: 'email', message: 'Địa chỉ email không hợp lệ'
            }
          ]
        },
        propsInput: {
          placeholder: 'Email',
          disabled: true
        }
      },
      {
        field: 'phoneNumber',
        type: 'text',
        label: 'Số điện thoại',
        getFieldDecoratorOpts: {
          initialValue: data.phoneNumber || '',
          rules: [
            { required: true, message: 'Vui lòng nhập số điện thoại' },
            {
              pattern: /((09|03|07|08|05)+([0-9]{8})\b)/g,
              message: 'Số điện thoại không hợp lệ'
            }
          ]
        },
        propsInput: {
          placeholder: 'Số điện thoại'
        }
      },
      {
        field: 'address',
        type: 'text',
        label: 'Địa chỉ',
        getFieldDecoratorOpts: {
          initialValue: data.address || '',
          rules: [
            { required: true, message: 'Vui lòng nhập địa chỉ' }
          ]
        },
        propsInput: {
          placeholder: 'Địa chỉ'
        }
      }
    ].map((item, idx) => (
      <Form.Item key={`${idx}`} label={item.label}>
        {getFieldDecorator(item.field, {
          ...item.getFieldDecoratorOpts
        })(
          item.type === 'text'
            ? <Input {...item.propsInput} />
            : item.type === 'radio'
              ? (
                <Radio.Group>
                  <Radio value='Nam'>Nam</Radio>
                  <Radio value='Nữ'>Nữ</Radio>
                  <Radio value='Khác'>Khác</Radio>
                </Radio.Group>
              )
              : item.type === 'password'
                ? <Input.Password {...item.propsInput} />
                : null
        )}
      </Form.Item>
    ))
  
    return (
      <Modal
        visible={this.state.visible}
        title='Cập nhật thông tin cá nhân'
        footer={null}
        onCancel={() => this.setVisibleDialog(false)}
      >
        <Form>
          {formItems}
        </Form>
        <div className='footerCustom'>
          <Button disabled={hasErrors(getFieldsError(), getFieldsValue())} loading={this.state.loading} type='primary' onClick={this.handleSubmit}>
            Cập nhật
          </Button>
        </div>
      </Modal>
    )
  }
}

export default Form.create()(InfoDialog)