import React, { useState } from 'react'
import { withRouter, Link } from 'react-router-dom'
import gql from 'graphql-tag'
import { Row, Select, Pagination, Empty } from 'antd'

import ShopItem from '../../components/ShopItem'
import { HOCQueryMutation } from '../../components/util'
import { DISTRICTS } from '../../_functions'

import './index.less'

const { Option } = Select

function SearchResult (props) {
  const [loading, setLoading] = useState(false)
  const [currentPage, setCurrentPage] = useState(1)
  const [filterValue, setFilterValue] = useState('')

  const handleChange = (value) => {
    setLoading(true)
    setCurrentPage(1)
    setFilterValue(value)
    props.data.refetch({
      txtSearch: props.match.params.txtSearch,
      txtFilter: value,
      limit: 18,
      offset: 0
    })
    setLoading(false)
  }

  const handleChangePage = (page) => {
    setLoading(true)
    setCurrentPage(page)
    props.data.refetch({
      txtSearch: props.match.params.txtSearch,
      txtFilter: filterValue,
      limit: 18,
      offset: (+page - 1) * 18
    })
    setLoading(false)
  }

  const imageUrl = process.env.REACT_APP_imagesUrl

  return (
    <div className='search-wrapper' style={{ minHeight: (window.innerHeight * (85 / 100)), background: '#d7dadb' }}>
      <div className='district-select'>
        <Select 
          defaultValue=''
          style={{ width: 200 }}
          onChange={handleChange} 
        >
          {DISTRICTS.map(district => (<Option key={district.value} value={district.value}>{district.name}</Option>))}
        </Select>
      </div>
      <div className='search-content' style={{ minHeight: window.innerHeight * (85 / 100) - 10 }}>
        <Row style={{ minHeight: '100%' }}>
          {props.data.shops.length > 0 ? props.data.shops.map((item, index) => (
            <Link key={index} to={`/shop/${item._id}`}>
              <ShopItem span={4} name={item.name} address={item.address} image={`${imageUrl}/${item.imageUrl}`} key={index} />
            </Link>
          )) : (
            <div style={{ marginTop: '15%' }}>
              <Empty description='Không có kết quả' />
            </div>
          )}  
        </Row>
      </div>
      {props.data.numberOfShop > 0 && (
        <div className='pagination-wrapper'>
          <Pagination current={currentPage} total={props.data.numberOfShop} pageSize={18} onChange={(page) => handleChangePage(page)} />
        </div>
      )}
    </div>
  )
}

const SHOPS = gql`
  query ($limit: Int!, $offset: Int!, $txtSearch: String, $txtFilter: String) {
    shops (limit: $limit, offset: $offset, txtSearch: $txtSearch, txtFilter: $txtFilter) {
      _id,
      name,
      imageUrl,
      address
    },
    numberOfShop (txtSearch: $txtSearch, txtFilter: $txtFilter)
  }
`

export default HOCQueryMutation([
  {
    query: SHOPS,
    name: 'shops',
    options: (props) => ({
      variables: {
        txtSearch: props.match.params.txtSearch,
        limit: 18,
        offset: 0
      }
    })
  }
])(withRouter(SearchResult))