import { hot } from 'react-hot-loader/root'
import React from 'react'
import {
  BrowserRouter,
  Switch,
  Route,
  Redirect
} from 'react-router-dom'
import { Provider, inject, observer } from 'mobx-react'
import { ApolloProvider, withApollo } from 'react-apollo'
import { Store, Client } from '../../tools'
import { LoadableComponent } from '../../components/util/loadable'
import Router from '../router'
// import './index.less'

@inject(store => store.store)
@observer
class Delivery extends React.Component {
  render () {
    const { isLogin } = this.props.Authen
    return (
      <BrowserRouter>   
        <Router />
      </BrowserRouter>
    )
  }
}

const store = new Store()

const App = () => (
  <Provider store={store}>
    <ApolloProvider client={Client}>
      <Delivery />
    </ApolloProvider>
  </Provider>
)

export default hot(App)
