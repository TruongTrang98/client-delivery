import React, { Component, Suspense } from 'react'
import { Switch, Redirect, Route, withRouter, useRouteMatch } from 'react-router-dom'
import { LoadableComponent } from '../../components/util/loadable'
import { Loading, HOCMobX, WrapLazy } from '../../components/util'
import Header from '../../components/Header'
import Footer from '../../components/Footer'

function Router (props) {
  const { Authen: { isLogin }, location: { pathname } } = props
  return (
    <>
      <Suspense fallback={<Loading />}>
        {pathname !== '/login' && pathname !== '/signup' && (<Header />)}
        <Switch>
          <Route
            exact
            path='/'
            component={LoadableComponent(import(`../home`))}
          />
          <Route
            exact
            path='/shop/:id'
            component={LoadableComponent(import(`../shopPage`))}
          />
          <Route
            exact
            path='/result/:txtSearch'
            component={LoadableComponent(import(`../searchResult`))}
          />
          {isLogin && (
            <Route
              exact
              path='/info'
              component={LoadableComponent(import(`../info`))}
            />
          )}
          {!isLogin && (
            <>
              <Route
                exact
                path='/signup'
                component={LoadableComponent(import(`../signUp`))}
              />
              <Route
                exact
                path='/login'
                component={LoadableComponent(import(`../login`))}
              />
            </>
          )}
          <Route component={() => <Redirect to='/' />} />
        </Switch>
        {pathname !== '/login' && pathname !== '/signup' && (<Footer />)}
      </Suspense>
    </>
  )
}

export default withRouter(HOCMobX(Router)(true))
