import React, { Component } from 'react'
import './ForgetPassword.css'
import { Form, Icon, Input, Button, Card } from 'antd'

class ForgetPassword extends Component {
  // state = {
  //   confirmDirty: false,
  //   autoCompleteResult: []
  // }

  handleSubmit = e => {
    e.preventDefault()
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values)
      }
    })
  }

  render () {
    const { getFieldDecorator } = this.props.form
    return (
      <div className='card-login'>
        <Card style={{ width: 360, height: 190 }}>
          <h1 style={{ marginBottom: 20 }}>QUÊN MẬT KHẨU</h1>
          <Form onSubmit={this.handleSubmit}>
            <Form.Item>
              {getFieldDecorator('email', {
                rules: [
                  {
                    type: 'email',
                    message: 'Email không đúng định dạng!'
                  },
                  {
                    required: true,
                    message: 'Vui lòng nhập Email!'
                  }
                ]
              })(
                <Input
                  prefix={
                    <Icon type='user' style={{ color: 'rgba(0,0,0,.25)' }} />
                  }
                  placeholder='Tên đăng nhập (Email)'
                />
              )}
            </Form.Item>
          
            <Form.Item>
              <Button
                type='primary'
                htmlType='submit'
                className='login-form-button'
              >
                Quên mật khẩu
              </Button>
            </Form.Item>
          </Form>
        </Card>
      </div>
    )
  }
}
const WrappedNormalForgetPassword = Form.create({ name: 'normal_login' })(ForgetPassword)

export default WrappedNormalForgetPassword
