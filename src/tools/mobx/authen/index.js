import { observable, action } from 'mobx'

class Authen {
  @observable isLogin = localStorage.getItem('TOKEN') || false
  @observable userProfile = {}
  @observable decodedToken = {}
  @observable posPath = ''

  @action onLogin = (token) => {
    this.isLogin = true
    localStorage.setItem('TOKEN', token)
    localStorage.setItem('path', 'user')
  }
  @action onLogout = () => {
    this.isLogin = false
    localStorage.removeItem('TOKEN')
    localStorage.removeItem('path')
    localStorage.clear()
  }
  @action getProfile = (profile) => {
    this.userProfile = profile
  }

  @action
  injectDecodedToken (data) {
    this.decodedToken = data
  }

  @action onPosPath = (path) => this.posPath = path
}

export { Authen }
