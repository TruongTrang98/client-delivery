const listPrivateRoute = [
  {
    displayName: 'Trang chủ',
    path: '/home',
    component: 'home',
    exact: true
  },
  {
    displayName: 'Shop',
    path: '/shop:id',
    component: 'shopPage',
    exact: true
  },
  {
    displayName: 'Thông tin cá nhân',
    path: '/info',
    component: 'info',
    exact: true
  }
]
  
export { listPrivateRoute }