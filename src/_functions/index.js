export const DISTRICTS = [
  {
    name: 'Tất cả',
    value: ''
  },
  {
    name: 'Quận 1',
    value: 'quan 1'
  },
  {
    name: 'Quận 2',
    value: 'quan 2'
  },
  {
    name: 'Quận 3',
    value: 'quan 3'
  },
  {
    name: 'Quận 4',
    value: 'quan 4'
  },
  {
    name: 'Quận 5',
    value: 'quan 5'
  },
  {
    name: 'Quận 6',
    value: 'quan 6'
  },
  {
    name: 'Quận 7',
    value: 'quan 7'
  },
  {
    name: 'Quận 8',
    value: 'quan 8'
  },
  {
    name: 'Quận 9',
    value: 'quan 9'
  },
  {
    name: 'Quận 10',
    value: 'quan 10'
  },
  {
    name: 'Quận 11',
    value: 'quan 11'
  },
  {
    name: 'Quận 12',
    value: 'quan 12'
  },
  {
    name: 'Quận Thủ Đức',
    value: 'thu duc'
  },
  {
    name: 'Quận Bình Thạnh',
    value: 'binh thanh'
  },
  {
    name: 'Quận Gò Vấp',
    value: 'go vap'
  },
  {
    name: 'Quận Phú Nhuận',
    value: 'phu nhuan'
  },
  {
    name: 'Quận Tân Phú',
    value: 'tan phu'
  },
  {
    name: 'Quận Bình Tân',
    value: 'binh tan'
  },
  {
    name: 'Quận Tân Bình',
    value: 'tan binh'
  },
  {
    name: 'Huyện Nhà Bè',
    value: 'nha be'
  },
  {
    name: 'Huyện Bình Chánh',
    value: 'binh chanh'
  },
  {
    name: 'Huyện Hóc Môn',
    value: 'hoc mon'
  },
  {
    name: 'Huyện Củ Chi',
    value: 'cu chi'
  },
  {
    name: 'Huyện Cân Giờ',
    value: 'can gio'
  }
]