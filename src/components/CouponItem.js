import React from 'react'
import { Card, Row, Col } from 'antd'

const listData = [
  {
    href: '',
    title: 'Gà cơ bắp',
    avatar: 'https://gw.alipayobjects.com/zos/rmsportal/mqaQswcyDLcXyDKnZfES.png',
    description: 'sas',
    conten: 'content'
  }
]

const imageLink = process.env.REACT_APP_imagesUrl

function CouponItem (props) {
  const { content, percent, shop: { _id, name, address, imageUrl, phoneNumber } } = props
  return (
    <Card>
      <Row style={{ marginTop: 10, marginBottom: 10 }}>
        <Col span={16}>
          <h1 style={{ color: 'blue', fontSize: 20 }}>{name}</h1>
          <p>{address}</p>
          <p>
            SĐT: &nbsp;
            {phoneNumber}
          </p>
          <p>
            Chương trình giảm giá: &nbsp;
            <span style={{ color: '#FD3018', fontSize: 18, fontWeight: 'bold' }}>{content}</span>
          </p>
          {/* <p>
            Phần trăm giảm giá: &nbsp;
            <span style={{ color: 'red', fontSize: 18, fontWeight: 'bold' }}>
              {percent}
              %
            </span>
          </p> */}
          <div style={{ marginTop: -138, marginLeft: 410 }}>
            <p style={{ fontSize: 35, color: '#FD3018', fontWeight: 'bold', marginBottom: 16, marginLeft: -33 }}>
              {percent}
              %
            </p>
            <img src='../../coupon.png' alt='coupon' width='65' style={{ marginLeft: -32, marginTop: -28 }} />
          </div>
        </Col>
        <Col span={8}>
          <img
            width={240}
            height={130}
            alt='logo'
            src={`${imageLink}/${imageUrl}`}
          />
        </Col>
      </Row>
    </Card>
  )
}

export default CouponItem
