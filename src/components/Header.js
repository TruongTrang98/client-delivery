import React, { Component } from 'react'
import { User, LogOut, Lock } from 'react-feather'
import { Button, Row, Col, Dropdown, Icon, Menu, Input } from 'antd'
import { withRouter } from 'react-router-dom'
import * as diacritic from 'diacritic'
import { HOCMobX } from './util'
import ChangePasswordModal from './modal/changePasswordModal'
import './Header.css'

let refChangePassword = null

function Header (props) {
  const { isLogin, onLogout } = props.Authen

  const getRefChangePassword = (value) => {
    refChangePassword = value
  }

  const menu = (
    <Menu>
      <Menu.Item key='1' onClick={() => props.history.push('/info')}>
        <User size={18} style={{ marginRight: '8px' }} /> 
        Thông tin cá nhân
      </Menu.Item>
      <Menu.Item 
        key='2'
        onClick={() => {
          if (refChangePassword) {
            refChangePassword.setState({ visible: true })
          }
        }}
      >
        <Lock size={18} style={{ marginRight: '8px' }} /> 
        Đổi mật khẩu
      </Menu.Item>
      <Menu.Item key='3' onClick={onLogout}>
        <LogOut size={18} style={{ marginRight: '8px' }} /> 
          Đăng xuất
      </Menu.Item>
    </Menu>
  )

  const iconclick = () => {
    if (props.location.pathname !== '/') {
      props.history.push('/')
    }
  }

  const onSearch = (value) => {
    const txtSearch = diacritic.clean(value).toLowerCase()
    props.history.push(`/result/${txtSearch}`)
  }

  const imageUrl = process.env.REACT_APP_imagesUrl

  return (
    <div className='header' style={{ borderBottom: '1px solid gray' }}>
      <div className='container-header'>
        <ChangePasswordModal getRef={getRefChangePassword} />
        <Row>
          <Col span={3}>
            { /* eslint-disable-next-line */ }
            <h1 className='logo' style={{ marginTop: 10, cursor: 'pointer' }} onClick={iconclick}>
              <img src={`${imageUrl}/logo.png`} alt='logo' width='160px' />
            </h1>
          </Col>
          <Col span={12} />
          <Col span={5} style={{ marginTop: '23px' }}>
            <Input.Search placeholder='Tìm kiếm' onSearch={(value) => onSearch(value)} />
          </Col>
          <Col span={4}>
            <div style={{ float: 'right', marginTop: 25 }}>
              {!isLogin ? (
                <>
                  <Button type='primary' ghost style={{ marginRight: 5 }} onClick={() => props.history.push('/login')}>
                    Đăng nhập
                  </Button>
                  <Button type='primary' ghost onClick={() => props.history.push('/signup')}>
                    Đăng ký
                  </Button>
                </>
              ) : (
                <>
                  {/* <Icon type='shopping-cart' style={{ fontSize: '20px', marginRight: '40px', cursor: 'pointer' }} /> */}
                  <Dropdown placement='bottomRight' overlay={menu}>
                    <Icon type='user' style={{ fontSize: '20px', cursor: 'pointer' }} />
                  </Dropdown>
                </>
              )}
            </div>
          </Col>
        </Row>
      </div>
    </div>
  )
}

export default withRouter(HOCMobX(Header)(true))