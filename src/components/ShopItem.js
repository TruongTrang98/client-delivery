import React from 'react'
import { Card, Col } from 'antd'

import './index.less'

const { Meta } = Card

const ShopItem = (props) => {
  function abc () {
    console.log('abc')
  }
  const { name, image, address } = props
  return (
    <div className='shop-item-content'>
      <Col span={props.span} style={{ marginTop: 10 }}>
        <Card
          style={{ margin: '0 5px 0 5px' }}
          hoverable
          cover={
            (
              <img
                alt='example'
                src={image}
                style={{ height: 130 }}
              />
            )
          }
        >
          <Meta
            title={name}
            description={address}
          />
        </Card>
      </Col>
    </div>
  )
}

export default ShopItem
