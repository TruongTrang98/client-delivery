import React, { Component } from 'react'
import { Modal, Form, Input } from 'antd'
import gql from 'graphql-tag'
import { HOCQueryMutation, Notify } from '../../util'

const CHANGE_PASSWORD = gql`
  mutation ($input: ChangePasswordInput!){
    changePassword(input: $input)
  }
`

class ChangePasswordModal extends Component {
  constructor (props) {
    super(props)
    this.state = {
      visible: false,
      setLoading: false
    }
  }

  componentDidMount = () => {
    this.props.getRef(this)
  }

  setModal = value => {
    this.setState({ visible: value })
  }

  handleChangePassword = async () => {
    try {
      this.setState({
        setLoading: true
      })
      const { data, refetch, mutate } = this.props
      const { getFieldsValue } = this.props.form
      const { oldPassword, newPassword, reNewPassword } = getFieldsValue()
      if (newPassword !== reNewPassword) {
        return new Notify('error', 'Xác nhận mật khẩu không trùng khớp!')
      }
      const res = await mutate.changePassword({
        variables: {
          input: {
            oldPassword,
            newPassword
          }
        }
      })

      if (res.data.changePassword === '200') {
        // await refetch()
        this.setState({
          setLoading: true
        })
        this.setModal(false)
        this.props.form.resetFields()
        return new Notify('success', 'Đổi mật khẩu thành công')
      } else {
        this.setState({
          setLoading: false
        })
        // return new Notify('success', 'Đổi mật khẩu thành công')
        return new Notify('error', 'Đổi mật khẩu thất bại')
      }
    } catch (error) {
      console.error('Error: ', error)
      this.setState({
        setLoading: false
      })
      return new Notify('error', 'Đổi mật khẩu thất bại')
    }
  }

  handleHideModal = () => {
    this.setModal(false)
    this.props.form.resetFields()
  }

  validateToNextPassword = (rule, value, callback) => {
    const { form } = this.props
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true })
    }
    if (!/^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{8,}$/.test(value)) {
      callback('Mật khẩu phải ít nhất 8 ký tự, bao gồm cả số và chữ cái')
    }
    callback()
  }

  compareToFirstPassword = (rule, value, callback) => {
    const { form } = this.props
    if (value && value !== form.getFieldValue('newPassword')) {
      callback('Mật khẩu không trùng khớp')
    } else {
      callback()
    }
  }

  render () {
    const { getFieldDecorator } = this.props.form
    return (
      <Modal
        title='Đổi mật khẩu'
        visible={this.state.visible}
        okText='Đổi mật khẩu'
        onOk={this.handleChangePassword}
        onCancel={this.handleHideModal}
        width='390px'
      >
        <Form>
          <Form.Item>
            {getFieldDecorator('oldPassword', {
              rules: [
                {
                  required: true,
                  message: 'Vui lòng nhập mật khẩu cũ của bạn!'
                }
              ]
            })(<Input.Password placeholder='Nhập mật khẩu cũ' onChange={(e) => this.setState({ oldPassword: e.target.value })} value={this.state.oldPassword} />)}
          </Form.Item>
          <Form.Item
            hasFeedback
            // help='Mật khẩu phải ít nhất 8 ký tự, bao gồm cả số và chữ cái'
            style={{ textAlign: 'left' }}
          >
            {getFieldDecorator('newPassword', {
              rules: [
                {
                  required: true,
                  message: 'Vui lòng nhập mật khẩu mới của bạn!'
                },
                {
                  validator: this.validateToNextPassword
                }
              ]
            })(<Input.Password placeholder='Nhập mật khẩu mới' onChange={(e) => this.setState({ newPassword: e.target.value })} value={this.state.newPassword} />)}
          </Form.Item>
          <Form.Item
            hasFeedback
            style={{ textAlign: 'left' }}
          >
            {getFieldDecorator('reNewPassword', {
              rules: [
                {
                  required: true,
                  message: 'Vui lòng xác nhận mật khẩu mới'
                },
                {
                  validator: this.compareToFirstPassword
                }
              ]
            })(<Input.Password onBlur={this.handleConfirmBlur} placeholder='Xác nhận mật khẩu mới' onChange={(e) => this.setState({ reNewPassword: e.target.value })} value={this.state.reNewPassword} />)}
          </Form.Item>
        </Form>
      </Modal>
    )
  }
}

export default HOCQueryMutation([
  {
    mutation: CHANGE_PASSWORD,
    name: 'changePassword',
    options: {}
  }
])(Form.create()(ChangePasswordModal))