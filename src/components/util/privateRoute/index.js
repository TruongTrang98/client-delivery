/* eslint-disable */
import React from 'react'
import { inject, observer } from 'mobx-react'
import { Route, Redirect } from 'react-router-dom'
import Scrollbars from 'react-custom-scrollbars'

const PrivateRoute = inject(store => store.store)(
  observer(({ Authen, profile, route, component: Component, path, ...rest }) => {
    const { isLogin } = Authen
    return (
      <Route
        {...rest}
        render={props => {
          if (isLogin) {
            if (path === '/profile/:ID') {
              return <Component route={route} path={path} {...props} {...rest} profile={profile} />
            } else {
              return (
                <Scrollbars style={{ width: '100vw', height: 'calc(100vh - 69px)' }} autoHide>
                  <Component route={route} path={path} {...props} {...rest} profile={profile} />
                </Scrollbars>
              )
            }
          } 
          // else {
          //   return <Redirect to='/home' />
          // }
        }}
      />
    )
  })
)

export { PrivateRoute }
