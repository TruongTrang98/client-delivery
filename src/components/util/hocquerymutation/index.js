/*eslint-disable*/
import React from 'react'
import { Skeleton } from 'antd'
import { withApollo as withApo, graphql } from 'react-apollo'
import compose from 'lodash/flowRight'

// data = [
//   { query: ``, variables: {} },
//   { query: ``, options: (props) => ({}) },
//   { mutation: ``, name: '', options: {} }
// ]

const HOCQueryMutation = data => Component => {
  if (data === undefined) return withApo(Component)
  else {
    try {
      let k = -1
      const GraphQLComponent = data.map((QueryOrMutate, idx) => {
        if (QueryOrMutate.query) {
          if (!!QueryOrMutate.variables) {
            return graphql(QueryOrMutate.query, {
              variables: QueryOrMutate.variables,
              skip: QueryOrMutate.skip,
              options: { fetchPolicy: 'network-only' }
            })
          } else if (!!QueryOrMutate.options) {
            return graphql(QueryOrMutate.query, { skip: QueryOrMutate.skip, options: QueryOrMutate.options, fetchPolicy: 'network-only' })
          }
          return graphql(QueryOrMutate.query)
        } else {
          if (k === -1) k = idx
          return graphql(QueryOrMutate.mutation, { name: QueryOrMutate.name, options: QueryOrMutate.options, skip: QueryOrMutate.skip  })
        }
      })
      const WrapComponent = props => {
        if (!!props.data) {
          const { loading, error } = props.data
          if (error) {
            console.log('HOCQueryMutation error ', error)
            return <Skeleton active />
          }
          if (loading) return <Skeleton active />
        }
        let wrapProps = { ...props }
        // eslint-disable-next-line no-unused-vars
        for (let QueryOrMutate of data) {
          if (QueryOrMutate.mutation) {
            wrapProps.mutate = { ...wrapProps.mutate, [QueryOrMutate.name]: props[QueryOrMutate.name] }
            delete wrapProps[QueryOrMutate.name]
          }
        }
        return <Component {...wrapProps} />
      }
      return compose(
        ...GraphQLComponent,
        withApo
      )(WrapComponent)
    } catch (e) {
      return withApo(Component)
    }
  }
}

export { HOCQueryMutation }
