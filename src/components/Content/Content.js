import React, { Component } from 'react'
import { Row, Col } from 'antd'
import LeftSide from './LeftSide'
import RightSide from './RightSide'
import './Content.css'

function Content (props) {
  function abc () {
    console.log('abc')
  }
  return (
    <div className='content' style={{ overflowY: 'auto' }}>
      <div className='container' style={{ maxWidth: 1284, margin: '0 auto' }}>
        <Row>
          <Col span={15}>
            <LeftSide />
          </Col>
          <Col span={9}>
            <RightSide />
          </Col>
        </Row>
      </div>
    </div>
  )
}

export default Content