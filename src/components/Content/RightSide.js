import React, { Component } from 'react'
import { withRouter, Link } from 'react-router-dom'
import { Input, Button } from 'antd'
import * as diacritic from 'diacritic'

const { Search } = Input

function RightSide (props) {
  function onSearch (value) {
    const txtSearch = diacritic.clean(value).toLowerCase()
    props.history.push(`/result/${txtSearch}`)
  }

  return (
    <div style={{ padding: 20, paddingTop: 60, paddingBottom: 80 }}>
      <h1 style={{ fontWeight: 'bold', paddingBottom: 20, fontSize: 35, color: 'white' }}>Tìm kiếm món ăn, cửa hàng yêu thích một cách nhanh chóng</h1>
      <Search placeholder='Tìm kiếm món ăn, cửa hàng' size='large' onSearch={value => onSearch(value)} enterButton />
      <div className='search-recommend' style={{ width: '100%', margin: '6% 0% 0% 0%' }}>
        <Link to={`/result/com suon`}>
          <Button ghost size='large' style={{ marginRight: 16, marginTop: 20 }}>Cơm sườn</Button>
        </Link>
        <Link to={`/result/com tam`}>
          <Button ghost size='large' style={{ marginLeft: 4, marginRight: 16, marginTop: 20 }}>Cơm tấm</Button>
        </Link>
        <Link to={`/result/ga`}>
          <Button ghost size='large' style={{ marginLeft: 4, marginRight: 10, marginTop: 20 }}>Món gà</Button>
        </Link>
        <Link to={`/result/vit`}> 
          <Button ghost size='large' style={{ marginLeft: 16, marginTop: 20 }}>Món vịt</Button>
        </Link>
        <Link to={`/result/bo`}> 
          <Button ghost size='large' style={{ marginRight: 16, marginTop: 20 }}>Món bò</Button>
        </Link>
        <Link to={`/result/pho`}> 
          <Button ghost size='large' style={{ marginLeft: 6, marginRight: 14, marginTop: 20 }}>Phở</Button>
        </Link>
        <Link to={`/result/mi`}> 
          <Button ghost size='large' style={{ marginLeft: 6, marginRight: 14, marginTop: 20 }}>Mì</Button>
        </Link>
        <Link to={`/result/coffee`}> 
          <Button ghost size='large' style={{ marginLeft: 11, marginRight: 10, marginTop: 20 }}>Coffee</Button>
        </Link>
        <Link to={`/result/sushi`}> 
          <Button ghost size='large' style={{ marginLeft: 16, marginTop: 20 }}>SuShi</Button>
        </Link>
        <Link to={`/result/pizza`}> 
          <Button ghost size='large' style={{ marginRight: 16, marginTop: 20 }}>Pizza</Button>
        </Link>
        <Link to={`/result/trang mieng`}> 
          <Button ghost size='large' style={{ marginRight: 16, marginTop: 20 }}>Tráng miệng</Button>
        </Link>
        <Link to={`/result/tra sua`}> 
          <Button ghost size='large' style={{ marginRight: 10, marginTop: 20 }}>Trà sữa</Button>
        </Link>    
        <Link to={`/result/mon an vat`}> 
          <Button ghost size='large' style={{ marginLeft: 16, marginTop: 20 }}>Món ăn vặt</Button>
        </Link>
      </div>
    </div>
  )
}

export default withRouter(RightSide)