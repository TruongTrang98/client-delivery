import React, { Component } from 'react'
import './LeftSide.css'
import { Select, Card, Row, Col, Button, Icon, Empty } from 'antd'
import { Link } from 'react-router-dom'
import gql from 'graphql-tag'
import CouponItem from '../CouponItem'
import ShopItem from '../ShopItem'
import { HOCQueryMutation } from '../util'
import { DISTRICTS } from '../../_functions'

const { Meta } = Card
const { Option } = Select

const SHOPS = gql`
query shops ($txtFilter: String, $limit: Int!) {
  shopsForUser(txtFilter: $txtFilter, limit: $limit){
    _id
    name
    imageUrl
    address
    phoneNumber
    email
    isActive
    account{
      _id
      username
      isLocked
      reason
    }
    createdAt
    updatedAt
  },
  couponsForUser {
    _id
    content
    percent
    shop {
      _id
      name
      address
      phoneNumber
      email
      imageUrl
    }
  }
}
`

function LeftSide (props) {
  const { data: { shopsForUser, couponsForUser, refetch } } = props

  // console.log(couponsForUser)

  localStorage.setItem('path', '/')

  console.log(shopsForUser)

  async function handleChange (value) {
    await props.data.refetch({
      txtFilter: value,
      limit: +12
    })
  }
  
  function onGeoSuccess (position) {
    console.log(position.coords.latitude, position.coords.longitude)
  }

  function onGeoError (error) {
    let detailError
    
    if (error.code === error.PERMISSION_DENIED) {
      detailError = 'User denied the request for Geolocation.'
    } else if (error.code === error.POSITION_UNAVAILABLE) {
      detailError = 'Location information is unavailable.'
    } else if (error.code === error.TIMEOUT) {
      detailError = 'The request to get user location timed out.'
    } else if (error.code === error.UNKNOWN_ERROR) {
      detailError = 'An unknown error occurred.'
    }
    console.log(detailError)
  }

  function getUserLocation () {
    let { geolocation } = navigator
    let options = {
      enableHighAccuracy: true,
      timeout: 5000,
      maximumAge: 0
    }
    if (geolocation) {
      geolocation.getCurrentPosition(onGeoSuccess, onGeoError, options)
    } else {
      // Không hỗ trợ geolocation
      alert('Ko')
    }
  }

  const imageUrl = process.env.REACT_APP_imagesUrl

  return (
    <div style={{ padding: '10px 15px 15px 15px', marginBottom: 20 }} className='recommend'>
      {/* <Button style={{ position: 'relative', top: 69, zIndex: 1, right: -410 }} type='primary' onClick={getUserLocation}>
        <Icon type='environment' />
        Định vị vị trí của bạn
      </Button> */}
      <Select 
        defaultValue='' 
        style={{ position: 'relative', top: 69, zIndex: 1, right: -606, width: 150 }}
        onChange={handleChange} 
      >
        {DISTRICTS.map(district => (<Option key={district.value} value={district.value}>{district.name}</Option>))}
      </Select>
      <Card title='Gợi ý' bordered style={{ marginTop: 20 }}>
        {shopsForUser.length > 0 ? (
          <Row style={{ marginTop: 10 }}>
            {shopsForUser.map((item, index) => (
              <Link key={index} to={`/shop/${item._id}`}>
                <ShopItem span={6} name={item.name} address={item.address} image={`${imageUrl}/${item.imageUrl}`} key={index} />
              </Link>
            ))}          
          </Row>
        ) : (
          <Empty description='Không có shop để hiển thị' />
        )}
      </Card>

      <Card title='Ưu đãi hot' bordered={false} style={{ marginTop: 30, paddingBottom: 20 }}>
        <Row style={{ marginTop: 10 }}>
          {couponsForUser.map((item, index) => (
            <Link key={index} to={`/shop/${item.shop._id}`}>
              <CouponItem span={6} content={item.content} percent={item.percent} shop={item.shop} />
            </Link>
          ))}  
        </Row>
      </Card>
    </div>
  )
}

export default HOCQueryMutation([
  {
    query: SHOPS,
    name: 'shopsForUser',
    options: {
      variables: {
        limit: 12
      }
    }
  }
])(LeftSide)
